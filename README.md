# P5Js - ButtonsWidget

Easy solution for managing Html5 Canvas Buttons with p5js
* Horizontal - rotate = 1 | Vertical - rotate = 0
* Automatic Button Layout

## Usage Horizontal :
``` 
  let nbButtonsPerSlot = 10
  btnWidget = new ButtonWidget(
    posX=0,
    posY=0,
    width=((windowWidth-(windowWidth/nbButtonsPerSlot))/nbButtonsPerSlot),
    height=50,
    font="monospace",
    fontWeigth="bold",
    fontSize="15px",
    padding=0,
    rotate=1,
    backgroundColor="#72787e",
    scBgColor="#3b88c3"
  );
  for ( let i = 0 ;  i < 20 ; i++){
    btnWidget.addButton("test"+str(i),"Alert Btn "+str(i), ()=>{alert("it works "+str(i))})
  }
```
![rotate 0](https://gitlab.com/Raymondaud.Q/p5js-buttonswidget/-/raw/main/img/rotate1.png)

## Usage Vertical

``` 
  let nbButtonsPerSlot = 10
  btnWidget = new ButtonWidget(
    posX=0,
    posY=0,
    width=((windowWidth-(windowWidth/nbButtonsPerSlot))/nbButtonsPerSlot),
    height=50,
    font="monospace",
    fontWeigth="bold",
    fontSize="15px",
    padding=0,
    rotate=0,
    backgroundColor="#72787e",
    scBgColor="#3b88c3"
  );

  for ( let i = 0 ;  i < 20 ; i++){
    btnWidget.addButton("test"+str(i),"Alert Btn "+str(i), ()=>{alert("it works "+str(i))})
  }
```
![rotate 1](https://gitlab.com/Raymondaud.Q/p5js-buttonswidget/-/raw/main/img/rotate0.png)

